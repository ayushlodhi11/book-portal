class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  before_filter :authorize

  def authorize
      username = cookies[:username]
      is_admin = cookies[:is_admin]
      if !username.present? || !is_admin.present?
        redirect_to "/login" and return if params["action"] != "login"
      elsif params["action"] == "login"
        page = is_admin ? "/" : "/library"
          redirect_to page and return
      end
  end
end
