class Book < ActiveRecord::Base
  has_many :user_books, :foreign_key => :book_id
end
