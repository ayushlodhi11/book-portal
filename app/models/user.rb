class User < ActiveRecord::Base
  validates_uniqueness_of :username
  has_many :user_books
end
