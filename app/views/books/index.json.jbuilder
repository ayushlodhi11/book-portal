json.array!(@books) do |book|
  json.extract! book, :id, :name, :status
  json.url book_url(book, format: :json)
end
