# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

u = User.find_or_initialize_by(:username=> "admin");
u.password = "admin"
u.is_admin = true
u.save!

u = User.find_or_initialize_by(:username=> "user");
u.password = "user"
u.is_admin = false
u.save!


u = Book.find_or_initialize_by(:name=> "Harry Potter");
u.status = true
u.save!
u = Book.find_or_initialize_by(:name=> "Jungle Book");
u.status = true
u.save!
u = Book.find_or_initialize_by(:name=> "Game Of Thrones");
u.status = false
u.save!